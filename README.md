

## Requerment

- Docker 
- docker-compose

## install
- clone repository
- cd root-project/laradock
- cp .env.example .env
- docker-compose up -d nginx mysql elasticsearch
- docker-compose exec workspace bash
- cp .env.example .env
- composer update
- php artisan config:cache
- php artisan migrate
- php artisan db:seed
- php artisan elastic:migrate
- php artisan scout:import "Bundles\Job\Models\Job"


## Postman link
https://www.getpostman.com/collections/40f86a4c91b04fb789a5

##swager link


