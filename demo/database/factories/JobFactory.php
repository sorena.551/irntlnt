<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Bundles\Job\Models\Job;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Job::class, function (Faker $faker) {
    $education=[ 'BSc', 'MD', 'PhD','diploma'];
    $category=['Designer', 'accounting', 'health care', 'Laboratory', 'lawyer', 'social service', 'security service'];


    return [
        'title' => $faker->text(40),
        'category' => $category[$faker->numberBetween(0,6)],
        'max_age' => $faker->boolean(20)?$faker->numberBetween(40,50):null,
        'min_age' => $faker->boolean(20)?$faker->numberBetween(20,30):null,
        'education' => $education[$faker->numberBetween(0,3)],
        'gender' => $faker->boolean(40)?$faker->boolean()?'male':'female':null,
        'salary' => $faker->boolean(20)?$faker->numberBetween(1,20)*1000000:null,
        'location' => $faker->boolean(20)?$faker->city:null,
        'expired_at'=>\Carbon\Carbon::now()->addDay($faker->numberBetween(100,150)),
        'lived_at'=>\Carbon\Carbon::now()->addDay($faker->numberBetween(50,150)),
        'created_at'=>\Carbon\Carbon::now()->addDay($faker->numberBetween(50,100)),

    ];
});
