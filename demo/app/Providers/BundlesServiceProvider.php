<?php

namespace App\Providers;

use Bundles\Auth\Models\User;
use Bundles\Blog\Models\BlogPost;
use Bundles\Company\Models\CompanyProfile;
use Bundles\Job\Models\Job;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Ramsey\Uuid\Uuid;

class BundlesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

        $this->bindRouteParameters();
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
    }

    protected function bindRouteParameters(): void
    {
        // Patterns: UUID
        foreach ([

            'job',

        ] as $bind) {
            Route::pattern($bind, Uuid::VALID_PATTERN);
        }

        // Binders: withUnVerified Models
        foreach ([

            'job' => Job::class,

        ] as $key => $bind) {
            $binder = function ($value) use ($bind) {
                if (!Uuid::isValid($value)) {
                    return null;
                }
                return $bind::withUnReliable()->where('id', $value)->first();
            };
            Route::bind($key, $binder);
        }

        Route::bind(
            'candidate',
            function ($value) {
                if (!Uuid::isValid($value)) {
                    return null;
                }
                return User::withUnReliable()->onlyCandidates()->where('id', $value)->first();
            }
        );



    }
}
