<?php

use Illuminate\Support\Facades\Route;

Route::namespace('Bundles\Job\Http\Controllers')
    ->middleware(['api'])
    ->group(
        function (): void {
            Route::apiResource('/job', 'JobController');
        }
    );

