<?php

namespace Bundles\Job\Http\Resources;

use App\Models\Value;
use http\Url;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Redis;

class JobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {


        return [
            'title' => $this->title,
            'category' => $this->category,
            'education' => $this->education,
            'gender' => $this->gender,
            'location' => $this->location,
            'expired_at' => $this->expired_at,
            'lived_at' => $this->lived_at,
            'salary' =>$this->salary,
            'min_age' => $this->min_age,
            'max_age' => $this->max_age,
            'created_at' => $this->created_at,
        ];
    }

}
