<?php

namespace Bundles\Job\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'category' => 'required|string',
            'education' => 'required|string',
            'gender' => 'required|string',
            'location' => 'required|string',
            'expired_at' => 'required|string|date_format:"Y/m/d"',
            'lived_at' => 'required|string|date_format:"Y/m/d"',
            'salary' => 'required|int',
            'min_age' => 'present|nullable|int',
            'max_age' => 'present|nullable|int',
        ];
    }

}
