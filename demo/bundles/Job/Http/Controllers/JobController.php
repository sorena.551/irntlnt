<?php

namespace Bundles\Job\Http\Controllers;

use App\Http\Controllers\Controller;
use Bundles\Job\Http\Requests\CreateJobRequest;
use Bundles\Job\Http\Resources\JobResource;
use Bundles\Job\Models\Job;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;


class JobController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request)
    {
        //todo this part can be implement by factory pattern but because of time ignored
        $payload=$this->generatePayload($request->all());

//        dd($payload);

        if ($request->has('sort'))
        {
            switch ($request->input('sort'))
            {
                case 'salary_asc':
                    $sort_columnt='salary';
                    $sort_order='asc';
                    break;
                case 'salary_desc':
                    $sort_columnt='salary';
                    $sort_order='desc';
                    break;
                case 'lived_at_asc':
                    $sort_columnt='lived_at';
                    $sort_order='asc';
                    break;
                case 'lived_at_desc':
                    $sort_columnt='lived_at';
                    $sort_order='desc';
                    break;

            }
        }else{
            $sort_columnt='salary';
            $sort_order='asc';
        }

        if (($payload))
        {
            $jobs = Job::rawSearch()
                ->query(
                    $payload
                )
                ->sort($sort_columnt,$sort_order)
                ->paginate()->models();
        }else
        {
            $jobs=Job::orderBy($sort_columnt,$sort_order)->paginate();
        }

        return JobResource::collection($jobs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateJobRequest $request)
    {
        $job = Job::create($request->all());

        return response([
            'data' => new JobResource($job), 'meta' => ['code' => Response::HTTP_CREATED]
        ], Response::HTTP_CREATED);


    }


    /**
     * Display the specified resource.
     *
     * @param Job $job
     * @return JobResource
     */
    public function show(Job $job)
    {
        return new JobResource($job);
    }


    public function update(CreateJobRequest $request, Job $job)
    {

        $job->update($request->all());
        return response([
            'data' => new JobResource($job), 'meta' => ['code' => Response::HTTP_CREATED]
        ], Response::HTTP_CREATED);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Job $job
     * @return \Illuminate\Http\Response
     * @throws Exception
     */
    public function destroy(Job $job): \Illuminate\Http\Response
    {
        $job->delete();
        return response([
            'data' => null, 'meta' => ['code' => Response::HTTP_NO_CONTENT]
        ], Response::HTTP_NO_CONTENT);

    }

    private function generatePayload(array $all)
    {
        $payload=null;
        if (
            key_exists('gender',$all) or
            key_exists('category',$all) or
            key_exists('education',$all) or
            key_exists('location',$all) or
            key_exists('title',$all) or
            key_exists('created_at_from',$all) or
            key_exists('created_at_to',$all) or
            key_exists('expired_at_from',$all) or
            key_exists('expired_at_to',$all) or
            key_exists('max_age_from',$all) or
            key_exists('max_age_to',$all) or
            key_exists('min_age_from',$all) or
            key_exists('min_age_to',$all)
        ) {
            $payload = ['bool' => []];
            if (key_exists('title', $all)) {
                $payload['bool']['must'] = ['match' => ['title' => $all['title']]];

            }
            if (key_exists('gender', $all))
            {
                if (!key_exists('filter',$payload['bool']))
                {
                    $payload['bool']['filter']=[];
                }
                array_push($payload['bool']['filter'],[
                    'term'=>[
                        'gender'=>$all['gender']
                    ]
                ]);

            }
            if (key_exists('location', $all))
            {
                if (!key_exists('filter',$payload['bool']))
                {
                    $payload['bool']['filter']=[];
                }
                array_push($payload['bool']['filter'],[
                    'term'=>[
                        'location'=>$all['location']
                    ]
                ]);

            }
            if (key_exists('education', $all))
            {
                if (!key_exists('filter',$payload['bool']))
                {
                    $payload['bool']['filter']=[];
                }
                array_push($payload['bool']['filter'],[
                    'term'=>[
                        'education'=>$all['education']
                    ]
                ]);

            }
            if (key_exists('category', $all))
            {
                if (!key_exists('filter',$payload['bool']))
                {
                    $payload['bool']['filter']=[];
                }
                array_push($payload['bool']['filter'],[
                    'term'=>[
                        'category'=>$all['category']
                    ]
                ]);

            }


            if (key_exists('min_age_from', $all))
            {
                if (!key_exists('filter',$payload['bool']))
                {
                    $payload['bool']['filter']=[];
                }

                array_push($payload['bool']['filter'],[
                    'range'=>[
                        'min_age'=>[
                            'gte'=>$all['min_age_from'],
                        ]
                    ]
                ]);

            }
            if (key_exists('min_age_to', $all))
            {
                if (!key_exists('filter',$payload['bool']))
                {
                    $payload['bool']['filter']=[];
                }
                if (!key_exists('min_age_from', $all))
                {
                    array_push($payload['bool']['filter'],[
                        'range'=>[
                            'min_age'=>[
                                'lte'=>$all['min_age_to'],
                            ]
                        ]
                    ]);
                }else
                {
                    foreach ($payload['bool']['filter'] as $key=> $item)
                    {
                        if (key_exists('range',$item))
                        {
                            if (key_exists('min_age',$item['range']))
                            {

                                $payload['bool']['filter'][$key]['range']['min_age']['lte']=$all['min_age_to'];
                            }
                        }
                    }
                }

            }


            if (key_exists('max_age_from', $all))
            {
                if (!key_exists('filter',$payload['bool']))
                {
                    $payload['bool']['filter']=[];
                }

                array_push($payload['bool']['filter'],[
                    'range'=>[
                        'max_age'=>[
                            'gte'=>$all['max_age_from'],
                        ]
                    ]
                ]);

            }
            if (key_exists('max_age_to', $all))
            {
                if (!key_exists('filter',$payload['bool']))
                {
                    $payload['bool']['filter']=[];
                }
                if (!key_exists('max_age_from', $all))
                {
                    array_push($payload['bool']['filter'],[
                        'range'=>[
                            'min_age'=>[
                                'lte'=>$all['max_age_to'],
                            ]
                        ]
                    ]);
                }else
                {
                    foreach ($payload['bool']['filter'] as $key=> $item)
                    {
                        if (key_exists('range',$item))
                        {
                            if (key_exists('max_age',$item['range']))
                            {

                                $payload['bool']['filter'][$key]['range']['max_age']['lte']=$all['max_age_to'];
                            }
                        }
                    }
                }

            }


            if (key_exists('created_at_from', $all))
            {
                if (!key_exists('filter',$payload['bool']))
                {
                    $payload['bool']['filter']=[];
                }

                array_push($payload['bool']['filter'],[
                    'range'=>[
                        'created_at'=>[
                            'gte'=>$all['created_at_from'],
                            "format"=> "yyyy-MM-dd",
                        ]
                    ]
                ]);

            }
            if (key_exists('created_at_to', $all))
            {
                if (!key_exists('filter',$payload['bool']))
                {
                    $payload['bool']['filter']=[];
                }
                if (!key_exists('created_at_from', $all))
                {
                    array_push($payload['bool']['filter'],[
                        'range'=>[
                            'created_at'=>[
                                'lte'=>$all['created_at_to'],
                                "format"=> "yyyy-MM-dd",
                            ]
                        ]
                    ]);
                }else
                {
                    foreach ($payload['bool']['filter'] as $key=> $item)
                    {
                        if (key_exists('range',$item))
                        {
                            if (key_exists('created_at',$item['range']))
                            {

                                $payload['bool']['filter'][$key]['range']['created_at']['lte']=$all['created_at_to'];
                            }
                        }
                    }
                }

            }


            if (key_exists('expired_at_from', $all))
            {
                if (!key_exists('filter',$payload['bool']))
                {
                    $payload['bool']['filter']=[];
                }

                array_push($payload['bool']['filter'],[
                    'range'=>[
                        'expired_at'=>[
                            'gte'=>$all['expired_at_from'],
                            "format"=> "yyyy-MM-dd",
                        ]
                    ]
                ]);

            }
            if (key_exists('expired_at_to', $all))
            {
                if (!key_exists('filter',$payload['bool']))
                {
                    $payload['bool']['filter']=[];
                }
                if (!key_exists('expired_at_from', $all))
                {
                    array_push($payload['bool']['filter'],[
                        'range'=>[
                            'expired_at'=>[
                                'lte'=>$all['expired_at_to'],
                                "format"=> "yyyy-MM-dd",
                            ]
                        ]
                    ]);
                }else
                {
                    foreach ($payload['bool']['filter'] as $key=> $item)
                    {
                        if (key_exists('range',$item))
                        {
                            if (key_exists('expired_at',$item['range']))
                            {

                                $payload['bool']['filter'][$key]['range']['expired_at']['lte']=$all['expired_at_to'];
                            }
                        }
                    }
                }

            }






        }

        return $payload;
    }


}
