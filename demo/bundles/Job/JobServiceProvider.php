<?php

namespace Bundles\Job;

use Bundles\Job\Commands\FindJobByAnyCommand;
use Bundles\Job\Commands\StoreWhyReason;
use Bundles\Job\Listeners\ExpirySubscriber;
use Bundles\Job\Models\Job;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

class JobServiceProvider extends ServiceProvider
{


    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        $this->loadRoutes();
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations/');
//        $this->loadMigrationsFrom(__DIR__ . '/elastic/migrations/');


    }

    /**
     * Register services.
     */
    public function register(): void
    {
    }

    protected function loadRoutes(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
    }


}
