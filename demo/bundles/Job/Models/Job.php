<?php

namespace Bundles\Job\Models;
use Carbon\Carbon;
use ElasticScoutDriverPlus\CustomSearch;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Job extends Model
{
//    protected $dateFormat='yyyy-MM-DD HH:mm:ss';
    protected $dates=['lived_at','expired_at'];

    protected $fillable=[
        'title' ,
        'category',
        'education',
        'gender',
        'location',
        'expired_at',
        'lived_at',
        'salary',
        'min_age',
        'max_age',
    ];
    use Searchable,CustomSearch;

    public function toSearchableArray()
    {
        return [
            'title' => $this->title,
            'location' => $this->location,
            'category' => $this->category,
            'education' => $this->education,
            'gender' => $this->gender,
            'min_age' => $this->min_age,
            'max_age' => $this->max_age,
            'salary' => $this->salary,
            'lived_at' => $this->lived_at->toDateString(),
            'expired_at' => $this->expired_at->toDateString(),
            'created_at' => $this->created_at->toDateString(),
        ];
    }

}
