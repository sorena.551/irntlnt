<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('category');
            $table->integer('min_age')->nullable();
            $table->integer('max_age')->nullable();
            $table->string('education')->nullable();
            $table->unsignedBigInteger('salary')->nullable();
            $table->string('gender')->nullable();
            $table->string('location')->nullable();
            $table->dateTime('expired_at');
            $table->dateTime('lived_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
