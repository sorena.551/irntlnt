<?php
declare(strict_types=1);

use ElasticAdapter\Indices\Mapping;
use ElasticAdapter\Indices\Settings;
use ElasticMigrations\Facades\Index;
use ElasticMigrations\MigrationInterface;

final class CreateJobsIndex implements MigrationInterface
{
    /**
     * Run the migration.
     */
    public function up(): void
    {
        Index::create('jobs', function (Mapping $mapping, Settings $settings) {
            $mapping->text('title');
            $mapping->keyword('category');
            $mapping->keyword('location');
            $mapping->keyword('education');
            $mapping->keyword('gender');
            $mapping->integer('min_age');
            $mapping->integer('max_age');
            $mapping->integer('salary');
            $mapping->date('lived_at',['format'=>'yyyy-MM-DD']);
            $mapping->date('created_at',['format'=>'yyyy-MM-DD']);
            $mapping->date('expired_at',['format'=>'yyyy-MM-DD']);
            $settings->index([

                'sort.field'=>['salary','lived_at']

            ]);
        });
    }

    /**
     * Reverse the migration.
     */
    public function down(): void
    {
        Index::dropIfExists('users');
    }
}
